const path = require('path');

const publicPath = process.env.NODE_ENV === 'production' ? `${process.cwd()}/dist/` : '/';
console.log('[VUE-CONFIG] ', process.env.NODE_ENV, publicPath);

module.exports = {
  publicPath,
  configureWebpack: {
    devtool: 'source-map',
    resolve: {
      alias: {
        '@builder': path.join(__dirname, '/src/modules/builder'),
        '@dashboard': path.join(__dirname, '/src/modules/dashboard'),
      },
    },
    devServer: {
      disableHostCheck: true,
    },
  },
  chainWebpack: config => {
    config.plugins.delete('prefetch');
  },
};
