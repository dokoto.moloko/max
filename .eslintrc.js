module.exports = {
  root: true,
  env: {
    node: true
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@', './src'],
          ['@builder', './src/modules/builder'],
          ['@dashboard', './src/modules/dashboard'],
        ],
        extensions: ['.ts', '.js', '.jsx', '.json']
      }
    }
  },
  extends: ['prettier', 'plugin:vue/recommended', '@vue/airbnb'],
  rules: {
    'vue/max-attributes-per-line': 0,
    'vue/html-closing-bracket-newline': 0,
    'vue/singleline-html-element-content-newline': 0,
    'no-shadow': 0,
    'arrow-parens': 0,
    'object-shorthand': 0,
    'func-names': 0,
    'import/prefer-default-export': 0,
    'object-curly-newline': 0,
    'import/no-extraneous-dependencies': 0,
    'space-before-function-paren': 0,
    'class-methods-use-this': 0,
    'prefer-promise-reject-errors': 0,
    'no-else-return': 0,
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: ['currState', 'RAParam', 'scope', 'event', 'ev']
      }
    ]
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
};