/* eslint-disable no-console */
const path = require('path');
const { spawn } = require('child_process');
const electron = require('electron');
const express = require('express');
const serveStatic = require('serve-static');
const sniffer = require('./sniffer');
const { ENV } = require('./constants');

const { app, BrowserWindow, ipcMain, screen } = electron;

let mainWindow;
const winURL = process.env.NODE_ENV === 'development'
  ? 'http://localhost:8080/'
  : `file://${process.cwd()}/dist/index.html`;

const httpServer = express();
httpServer.use(serveStatic(ENV.ELEC_FILMS_DB_PATH, { maxAge: '10d' }));
httpServer.listen(ENV.VUE_APP_IMG_PORT);

// console.log('[ELECTRON] ', process.env.NODE_ENV, ENV, winURL);


function createWindow() {
  const mainScreen = screen.getPrimaryDisplay();
  mainWindow = new BrowserWindow({
    height: Number(ENV.ELEC_SCREEN_HEIGHT),
    width: mainScreen.size.width || Number(ENV.ELEC_SCREEN_WIDTH),
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true,
      preload: path.join(__dirname, '/preload.js'),
    },
  });
  // DEBUGGING
  mainWindow.webContents.openDevTools();
  mainWindow.loadURL(winURL);

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}

ipcMain.handle('open:video', async (event, args) => {
  console.log('open:video', args);
  const vlc = spawn(ENV.ELEC_VLC_PATH, [args.file, '--video-on-top', '--no-video-deco', '--no-embedded-video']);
  vlc.stdout.on('data', (data) => {
    return Promise.resolve(data);
  });
  vlc.stderr.on('data', (data) => {
    return Promise.resolve(data);
  });
  vlc.on('exit', (code) => {
    console.log(`Child exited with code ${code}`);
  });
});

ipcMain.handle('update:films:db', async (event, args) => {
  console.log('update:films:db', args);
  return sniffer.execute(ENV.ELEC_FILMS_DB_PATH)
    .then(db => db)
    .catch(error => {
      console.error(error);
    });
});

app.on('ready', createWindow);
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
