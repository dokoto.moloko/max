exports.ENV = {
  ELEC_SCREEN_WIDTH: 1280,
  ELEC_SCREEN_HEIGHT: 1024,
  ELEC_VLC_PATH: '/Applications/VLC.app/Contents/MacOS/VLC',
  ELEC_FILMS_DB_PATH: '/Volumes/almacen/VIDEO/PELICULAS',
  ELEC_DB_PATH: 'films.json',
  VUE_APP_TITLE: 'Max',
  VUE_APP_IMG_PORT: 3000,
};
