import Vue from 'vue';
import VueI18n from 'vue-i18n';

import es from './lang/es';
import en from './lang/en';

import dateTimeFormats from './datetime/datetime.json';

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en, es },
  dateTimeFormats,
});

export default i18n;
