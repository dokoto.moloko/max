export default {
  app: {
    title: 'Max',
    builder: {
      build_films: 'Build Films',
      delete_films: 'Delete Films DB',
      dashboard: 'Dashboard',
    },
  },
};
