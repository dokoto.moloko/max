import Vue from 'vue';
import i18n from '@/common/helpers/i18n';
import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
// https://blog.logrocket.com/full-guide-to-using-font-awesome-icons-in-vue-js-apps-5574c74d9b2d/
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSpinner, faQuestionCircle, faPlayCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faSpinner, faQuestionCircle, faPlayCircle);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  created() {
    this.$router.push('/');
  },
  render: (h) => h(App),
}).$mount('#app');
