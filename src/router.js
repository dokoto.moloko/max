import Vue from 'vue';
import VueRouter from 'vue-router';

const Builder = () => import(/* webpackChunkName: 'Builder' */ '@builder/Layout.vue');
const Dashboard = () => import(/* webpackChunkName: 'Dashboard' */ '@dashboard/Layout.vue');


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Builder',
    components: { main: Builder },
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    components: { main: Dashboard },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
