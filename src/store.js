import Vue from 'vue';
import Vuex from 'vuex';
import builder from '@builder/store';
import dashboard from '@dashboard/store';

Vue.use(Vuex);
const modules = {
  builder,
  dashboard,
};

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules,
});
